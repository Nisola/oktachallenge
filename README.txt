Website to obtain user information using Okta's API
**********************************************************************************************************
**BIG PROBLEM**
CROSS DOMAIN REQUESTS
Javascript is still hard-pressed when it comes to Cross-Domain Requests. 
Usually CORS would do the trick but after hours of tinkering before realizing session did not support CORS 
I failed to find an alternative in time to create a site that bypasses the cross domain requests in time. 
The alternative I know of, JSONP could not help me in the situation as the request type was POST.In retrospect I should have just given up on trying to make the JS work and used cURL via php instead.
**********************************************************************************************************


Preliminary Understanding 

	   1) Have User enter in their user-name and password (Credentails)
	   2) Submit credentials to ajax that is ties to login buttons' onSubmit function 
***WRONG*** 3) Use Primary authentication request to check if credentials are valid ***WRONG*** SEE BELOW
	   4a) If invalid, display error message and direct user to try again.
	   4b) On success authentication status, the user data will be retrieved via secondary request
	    	5) JSON object parsed to retrieve, users first name, Last Name and email 
	    	6) Name, and email displayed.


CHANGED 
Instead of using primary authentication, Session can be used.
$(document).ready(function() {


	/*set the response header*/


	/* This function uses the credentials supplied by user input to obtain user data using OKTA's API.
		It displays the first name last name and email or the user if the credentials are valid
		if credentials are invalid the end user sees a message why
	*/
	$(document).on("click", "#submit", function(){

		//User's inputed credentials (username and Login)
		var user =	$("#user").val();
		var pass =  $("#pass").val();

		// Safely encode parameters to escape special characters
		user = encodeURIComponent(user);
		pass = encodeURIComponent(pass);

		//create Json obj to send to server
		var authData = {username: user, password:pass};
		var homeLink= $("<a href='https://nisolashobayo.com/challenge'> <hi>Return Home<hi></a>");


		//ajax call to check validity of inputed credentials by creating a session
		$.ajax({
			url: "https://dev-241104.oktapreview.com/api/v1/sessions",
			method: "POST",
			headers: { Authorization : 'SSWS 00nWf98VMRjX8nSW6T0odYCZbHLvn3waF1ZMO8moUV'},
			data: JSON.stringify(authData), //serialize the autherntication data to JSON 
			dataType: "JSON", //API returns large JSON objects which are easy to parse through
			accept: 'application/json',
			success: function(data) {
				//checks if the call returns an oject with a status key and stores status else stores the error 
				if( data.userid != null){
					//get's the user's id from the returned Json object
					var id = data.userid;

						//retreives user data using call to Okta user API
						$.ajax({
							url :"https://dev-241104.oktapreview.com/api/v1/users/"+id,
							method: "GET",
					        accept: 'application/json',
							headers: { Authorization : 'SSWS 00nWf98VMRjX8nSW6T0odYCZbHLvn3waF1ZMO8moUV'},
							dataType: "JSON",
							success: function(userInfo) {
								
								//retrive fname lastName and email from returned JSON obj
								var firstName = userInfo.profile.firstname;
								var lastName = userInfo.profile.lastname;
								var email =  userInfo.profile.email;
								
								//concatinates information for display
								var finText = "Name:"+firstName+" "+lastName+"\n email:" + email;

								//replace login boxes with user's Name and Email
								$("#text-wrapper").empty();
								$("#text-wrapper").html(finText);
								
								//create a link to go back to inital login page
						        $("#text-wrapper").append(homeLink);
							}
						});
				}
				else{
					//replace login boxes with user's Name and Email
					$("#text-wrapper").empty();
					$("#text-wrapper").html(data.errorSummary);
					//create a link to go back to inital login page
			        $("#text-wrapper").append(homeLink);}
			}
		});
	});
});


